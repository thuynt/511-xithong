/*!
 * thuynt
 *
 *
 * @author thuynt
 * @version 2.0.0
 * Copyright 2021. MIT licensed.
 */
$(document).ready(function () {
  $(".loading-start").hide();
  $(window).resize(function () {
    var width = $(window).width();
    if (width <= 767) {
      $('.box-slogan-bottom').removeAttr('id', 'form-order-bottom');
    } else {
      $('.box-slogan-bottom').attr('id', 'form-order-bottom');
    }
  });
  var windowsize = $(window).width();

  if (windowsize < 768) {
    $('.box-slogan-bottom').removeAttr('id', 'form-order-bottom');
    $('.box-product-order').attr('id', 'form-order-bottom');
  }

  $('#slide-1').owlCarousel({
    lazyLoad: true,
    loop: false,
    margin: 15,
    nav: false,
    autoplay: true,
    autoplayTimeout: 7500,
    responsive: {
      0: {
        items: 1,
        mouseDrag: true,
        loop: true
      },
      600: {
        items: 2,
        mouseDrag: true,
        loop: true
      },
      1000: {
        items: 3,
        mouseDrag: false
      }
    }
  });
  $('#slide-2').owlCarousel({
    lazyLoad: true,
    loop: false,
    margin: 10,
    nav: false,
    autoplay: true,
    autoplayTimeout: 7500,
    responsive: {
      0: {
        items: 1,
        mouseDrag: true,
        loop: true
      },
      600: {
        items: 2,
        mouseDrag: true,
        loop: true
      },
      1000: {
        items: 3,
        mouseDrag: false
      }
    }
  });
  $('#slide-comment-mb').owlCarousel({
    lazyLoad: true,
    items: 1,
    loop: true,
    margin: 10,
    nav: false,
    autoplay: true,
    autoplayTimeout: 15000
  });
  $("#btn-close-ads").click(function () {
    $(".box-ads-header").addClass('close-ads');
  });
  $('.btn-send-info').click(function () {
    var username = $.trim($('#username').val());
    var phone_number = $.trim($('#phone_number').val());

    if (username == '') {
      $('#username_error').text('Không để trống!');
    } else {
      $('#username_error').text('').hide();
    }

    if (phone_number == '' || !/^[0-9]{10}$/.test(phone_number)) {
      $('#phone_error').text('Số điện thoại không đúng định dạng!');
    } else {
      $('#phone_error').text('');
    }
  });
}); // function validate form

$(function () {
  set_($('#number-max'), 100); //return 3 maximum digites

  function set_(_this, max) {
    var block = _this.parent();

    block.find(".increase").click(function () {
      var currentVal = parseInt(_this.val());

      if (currentVal != NaN && currentVal + 1 <= max) {
        _this.val(currentVal + 1);

        // $('#total-order').val((currentVal + 1) * parseInt(price));
        var num_price = (currentVal + 1) * parseInt(price);
        var price_num = num_price.toLocaleString();
        $('#total-order').val(price_num + ' VND');
      }
    });
    block.find(".decrease").click(function () {
      var currentVal = parseInt(_this.val());

      if (currentVal != NaN && currentVal > 1) {
        _this.val(currentVal - 1);

        // $('#total-order').val((currentVal - 1) * parseInt(price));
        var num_price = (currentVal - 1) * parseInt(price);
        var price_num = num_price.toLocaleString();
        $('#total-order').val(price_num + ' VND');
      }
    });
  }
});
$(function () {
  $(".input-number").keypress(function (e) {
    if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57)) {
      $("#errmsg").html("Chỉ nhập số").stop().show().fadeOut("slow");
      return false;
    }
  });
});

//update main js validate form
var messagesName = 'Họ tên bắt buộc';
var messagesPhone = 'Số điện thoại bắt buộc';
$(document).ready(function () {
  setCookieUtm();

  if (/Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent)) {
    messagesName = '*';
    messagesPhone = '*';
  }

  /* ------------------ form header ------------------ */
  let headerFields = [
    {name: 'fullName', required: true, messages: 'Họ tên bắt buộc'},
    {name: 'phone', required: true, messages: 'Số điện thoại bắt buộc'}
  ];
  createOpp("#ads-form-header", headerFields);

  /* ------------------ form info ------------------ */
  let infoFields = [
    {name: 'fullName', required: true, messages: 'Họ tên bắt buộc'},
    {name: 'phone', required: true, messages: 'Số điện thoại bắt buộc'}
  ];
  createOpp("#ads-form", infoFields);

  /* ------------------ form register ------------------ */
  let registerFields = [
    {name: 'fullName', required: true, messages: 'Họ tên bắt buộc'},
    {name: 'phone', required: true, messages: 'Số điện thoại bắt buộc'},
    {name: 'description', required: false, messages: ''}
  ];
  createOpp("#form-register", registerFields);

  /* ------------------ form form-order ------------------ */
  let orderFields = [
    {name: 'fullName', required: true, messages: 'Họ tên bắt buộc'},
    {name: 'phone', required: true, messages: 'Số điện thoại bắt buộc'},
    {name: 'address', required: true, messages: 'Địa chỉ bắt buộc'},
    {name: 'quantity', required: false, messages: ''}
  ];
  createOpp("#form-order", orderFields);

  $(document).on("click", "#close-modal", function () {
    $('#modal-notif').modal('hide');
  });

  $(document).on("change", "#number-max", function () {
    if ($(this).val() < 1) {
      $(this).val(1);
    }
    var num_price = parseInt($(this).val()) * parseInt(price);
    var price_num = num_price.toLocaleString();
    $('#total-order').val(price_num + ' VND');
  });
});

/**
 *
 * @param idForm : # + idFrom
 * @param fields
 * [{name: ten truwng, required: tru, messages: 'truonng bat buoc'}]
 */
function createOpp(idForm, fields) {
  if (fields.length > 0 && $(idForm).length > 0) {
    var rulesForm = {};
    var messagesError = {};
    for (const v of fields) {
      rulesForm[v.name] = {required: v.required};
      messagesError[v.name] = {required: v.messages};
    }

    $(idForm).validate({
      ignore: ":hidden",
      rules: rulesForm,
      messages: messagesError,
      submitHandler: function (form) {
        $('#loading-request').show();
        $(form).find('buton').prop('disabled', true);
        var dataPost = {};
        let utmCode = getCookieUtm('utm_code');
        if (utmCode === undefined) {
          utmCode = '';
        }

        dataPost.advertisingSource = utmCode;
        for (const v of fields) {
          dataPost[v.name] = $(idForm).find('.' + v.name).val();
        }

        $.ajax({
          type: "POST",
          url: "php/action.php",
          data: dataPost,
          success: function (response) {
            $('#loading-request').hide();
            $(form).find('buton').prop('disabled', false);
            if (response.success === true) {
              for (const v of fields) {
                if (v.name === 'quantity') {
                  $(idForm).find('.' + v.name).val(1);
                } else {
                  $(idForm).find('.' + v.name).val('');
                }
              }
              $('#modal-success').modal('show');
            } else {
              $('#modal-notif').modal('show');
            }
          }
        });
        return false;
      }
    });
  }
}

function setCookieUtm() {
  const domain = window.location.hostname;
  const queryString = window.location.search;
  const urlParams = new URLSearchParams(queryString);
  const utm_source = urlParams.get('utm_source');
  const utm_medium = urlParams.get('utm_medium');
  const utm_campaign = urlParams.get('utm_campaign');
  const utm_adgroup = urlParams.get('utm_adgroup');
  const utm_adset = urlParams.get('utm_adset');
  const utm_code = urlParams.get('utm_code');

  if (utm_source != null && utm_source != '') {
    $.cookie(domain + prefixKey + '-utm_source', utm_source, {
      expires: expiresCookie
    });
  }

  if (utm_medium != null && utm_medium != '') {
    $.cookie(domain + prefixKey + '-utm_medium', utm_medium, {
      expires: expiresCookie
    });
  }

  if (utm_campaign != null && utm_campaign != '') {
    $.cookie(domain + prefixKey + '-utm_campaign', utm_campaign, {
      expires: expiresCookie
    });
  }

  if (utm_adgroup != null && utm_adgroup != '') {
    $.cookie(domain + prefixKey + '-utm_adgroup', utm_adgroup, {
      expires: expiresCookie
    });
  }

  if (utm_source != null && utm_source != '') {
    $.cookie(domain + prefixKey + '_utm_source', utm_source, {
      expires: expiresCookie
    });
  }

  if (utm_adset != null && utm_adset != '') {
    $.cookie(domain + prefixKey + '-utm_adset', utm_adset, {
      expires: expiresCookie
    });
  }

  if (utm_code != null && utm_code != '') {
    $.cookie(domain + prefixKey + '-utm_code', utm_code, {
      expires: expiresCookie
    });
  }
}

function getCookieUtm(key) {
  const domain = window.location.hostname;
  return $.cookie(domain + prefixKey + '-' + key);
}